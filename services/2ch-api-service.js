/*
 * 2chWebm parser 
 * Module for making API calls 
 * to 2ch.hk endpoints
 */

//Dependences
const rp = require('request-promise');

//Contant routs
const dvachRoute = 'https://2ch.hk'
const allTrhreadsRoute = dvachRoute + '/b/threads.json'
const allPostsFromThreadRoute = function(num) {
    return dvachRoute+ '/b/res/'+num+'.json'
}
    


//Options object
let options = {
    uri: '',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true 
};


//Get all threads on /b
async function getAllThreads() {
    //set endpoint to settings
    options.uri = allTrhreadsRoute;
    try{
         //making apicall via request
        let {threads} =  await rp(options); 
        return threads
    } catch (err){
        return {}
    }   
}

//Get all all posts from thread
async function getAllPostsFromThread(num) {
    //set endpoint to settings
    options.uri = allPostsFromThreadRoute(num);
    try{
         //making apicall via request
        let posts =  await rp(options); 
        return posts.threads[0].posts 
    } catch (err){
        console.log(err);
        return []
    }   
}


async function getWebmThreads() {
    
    const maxSubjectLength = 63;
    let threads = await getAllThreads();
    //finding webm thread in array
    let webmThreads = threads.filter(function (thread) {
        let threadSubject = thread.subject.toLowerCase();
        return (threadSubject.indexOf("webm") >= 0 || threadSubject.indexOf("fap") >= 0 )
    })
    return webmThreads
}

async function getAllWebmFromThread(num){

    let webmArr = [];
    let posts = await getAllPostsFromThread(num);

    for(let p of posts)
        for(let f of p.files){
            if(f.path.indexOf('webm')>=0 || f.path.indexOf('mp4')>=0 )
                webmArr.push(dvachRoute+f.path);
        }
            
            

    return webmArr;

}

module.exports = {
    getWebmThreads:getWebmThreads,
    getAllWebmFromThread:getAllWebmFromThread
    
}